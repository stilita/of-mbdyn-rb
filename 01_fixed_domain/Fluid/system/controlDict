/*--------------------------------*- C++ -*----------------------------------*\
  =========                 |				
  \\      /  F ield         | OpenFOAM: The Open Source CFD Toolbox
   \\    /   O peration     | Website:  https://openfoam.org
    \\  /    A nd           | Version:  8                                   	
     \\/     M anipulation  |
\*---------------------------------------------------------------------------*/
FoamFile
{
    version     2.0;
    format      ascii;
    class       dictionary;
    object      controlDict;
}
// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //

application     interFoam;

startFrom       latestTime;

startTime       0;

stopAt          endTime;

endTime         4;

deltaT          0.001;

//writeControl    timeStep;
//writeControl    adjustableRunTime;
writeControl    runTime;

writeInterval   0.01;

purgeWrite      0;

writeFormat     ascii;

writePrecision  8;

writeCompression off;

timeFormat      general;

timePrecision   6;

runTimeModifiable yes;

adjustTimeStep  no;

maxCo           10;
maxAlphaCo      1; //For accurate surface tracking

//maxCo           20;
//maxAlphaCo      10;  //For fast solution - Good for mean values - For stability be sure to do at least 3 PISO loops

maxDeltaT       0.1;

// ************************************************************************* //

functions
{

///////////////////////////////////////////////////////////////////////////

    forces
    {
        type            forces;
        libs            ("libforces.so");
        patches         (hull);
        rhoInf          998.8;
        log             on;
        writeControl    timeStep;
        writeInterval   1;
        CofR            (0 0 0);
    }

///////////////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////////////////////

    forceCoeffs_object
    {
	// rhoInf - reference density
	// CofR - Centre of rotation
	// dragDir - Direction of drag coefficient
	// liftDir - Direction of lift coefficient
	// pitchAxis - Pitching moment axis
	// magUinf - free stream velocity magnitude
	// lRef - reference length
	// Aref - reference area
	type				forceCoeffs;
	libs				("libforces.so");
	//patches ("body1" "body2" "body3");
	patches			("hull");

	pName				p;
	Uname				U;
	rho				rhoInf;

	rhoInf				998.8;
	//rhoInf 907;

	//// Dump to file
	log				true;

	CofR				(0 0 0);
	liftDir			(0 0 1);
	dragDir			(1 0 0);
	pitchAxis			(0 1 0);
	magUInf			0.5;
	lRef				1;         // reference lenght for moments!!!
	Aref				1.0;         // reference area 1 for 2d

        writeControl			timeStep;
        writeInterval			1;
    }

///////////////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////////////////////

    probes_online
    {
        type				probes;
        libs				("libfieldFunctionObjects.so");
        enabled			true;

        //writeControl			outputTime;
        writeControl			timeStep;
        writeInterval			1;

	probeLocations  
	( 
	    (-0.21 0.0 -0.01)
	    (0.22 0 -0.01)
	    //(3 0 -0.05)
	);

	fields
	(   
 	    U 
	    p
	    p_rgh
	    alpha.water
	);

    }

///////////////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////////////////////

field_averages
{
    type				fieldAverage;
    libs				("libfieldFunctionObjects.so");
    enabled				true;

    writeControl			outputTime;
    //writeControl			timeStep;
    //writeInterval			100;

    //cleanRestart			true;

    timeStart				2;
    //timeEnd				5000;

    fields
    (
        U
        {
            mean        on;
            prime2Mean  off;
            base        time;
        }

        p
        {
            mean        on;
            prime2Mean  off;
            base        time;
        }

        p_rgh
        {
            mean        on;
            prime2Mean  off;
            base        time;
        }

        alpha.water
        {
            mean        on;
            prime2Mean  off;
            base        time;
        }
    );
}

///////////////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////////////////////

    yplus
    {
    	type				yPlus;
    	libs				("libutilityFunctionObjects.so");
    	enabled			true;
        writeControl			outputTime;
    }

///////////////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////////////////////

    minmaxdomain
    {
	type				fieldMinMax;

	libs				("libfieldFunctionObjects.so");

	enabled			true; //true or false

	mode				component;

	writeControl			timeStep;
	writeInterval			1;

	log				true;

	fields				(p p_rgh U alpha.water); // k omega nut);
    }

///////////////////////////////////////////////////////////////////////////

}

