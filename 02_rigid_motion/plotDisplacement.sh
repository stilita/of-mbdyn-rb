#! /bin/bash
gnuplot -p << EOF
set grid
set title 'Displacement of the Flap Tip'
set xlabel 'Time [s]'
set ylabel 'Displacement [m]'
plot for [col=8:10] "./precice-Solid-watchpoint-tip.log" using 1:col with lines title columnheader
EOF

